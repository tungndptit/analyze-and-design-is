package servlet.book;

import edu.ptit.entity.Book;
import edu.ptit.session.BookFacadeLocal;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import servlet.SessionKeys;
import utility.Func;

/**
 *
 * @author ndt
 */
@WebServlet("/ViewBook")
public class ViewBook extends HttpServlet {

    @EJB
    BookFacadeLocal bookDAO;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int idbook = Integer.parseInt(request.getParameter("idbook"));

        Book book = bookDAO.find(idbook);
//                getBookById(idbook, Func.getCurrentDateTime());
        HttpSession session = request.getSession();
        session.setAttribute(SessionKeys.BOOK.BOOK, book);
        response.sendRedirect("index/book/ViewBook.jsp");
    }
}
