/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.orders;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ptit.entity.Cart;
import edu.ptit.entity.Cartdetail;
import servlet.SessionKeys;

/**
 *
 * @author Admin
 */
@WebServlet(name = "ViewCart", urlPatterns = {"/ViewCart"})
public class ViewCart extends HttpServlet {

//    private static final String TAG = "ViewCart: ";
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        System.out.println(TAG + "Here is ViewCart Servlet");
        HttpSession session = request.getSession();
        Cart cart = (Cart) session.getAttribute(SessionKeys.CART);
        Collection<Cartdetail> cartdetails = cart.getCartdetailCollection();
        Float sumCost = 0f;
        for (Cartdetail cartdetail : cartdetails ) {
            sumCost += cartdetail.getQuantity() * cartdetail.getBookID().getCurrentPrice();
        }

        Float charge = new Float(sumCost);
//        ArrayList<CustomerSaleOff> cusSaleOffs = (ArrayList<CustomerSaleOff>) session.getAttribute(SessionKeys.CUSTOMER_SALE_OFF_LIST);
//        if (cusSaleOffs!=null) {
//            for (CustomerSaleOff cso : cusSaleOffs) {
//            charge = charge - (sumCost * cso.getPercent() / 100.0f) - cso.getPrice();
//            }
//        }
        charge = Math.max(0, charge);

        session.setAttribute(SessionKeys.SUM_COST, sumCost);
        session.setAttribute(SessionKeys.CHARGE, charge);

        response.sendRedirect("index/orders/Cart.jsp");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
