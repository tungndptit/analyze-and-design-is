package servlet.orders;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import edu.ptit.entity.Book;
import edu.ptit.entity.Cart;
import edu.ptit.entity.Cartdetail;
import edu.ptit.session.BookFacadeLocal;
import servlet.SessionKeys;
import utility.Func;

/**
 *
 * @author ndt
 */
@WebServlet("/AddToCart")
public class AddToCart extends HttpServlet {

    @EJB
    BookFacadeLocal bookDAO;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        synchronized (session) {
            String idStr = request.getParameter("idOrder").trim();
            int quantity = 1;
            try {
                quantity = Integer.parseInt(request.getParameter("bookQuantities").trim());
            } catch (Exception e) {
                quantity = 1;
            }

            Book b = bookDAO.find(Integer.parseInt(idStr));
            Cart cart = (Cart) session.getAttribute(SessionKeys.CART);
            if (cart == null) {
                cart = new Cart();
            }
            if (cart.getCartdetailCollection() == null){
                cart.setCartdetailCollection(new ArrayList<Cartdetail>());
            }

            boolean haveInCart = false;
            for (Iterator<Cartdetail> iterator = cart.getCartdetailCollection().iterator(); iterator.hasNext();){
                Cartdetail cartdetail = iterator.next();
                if (cartdetail.getBookID().getId() == b.getId()){
                    cartdetail.setQuantity(cartdetail.getQuantity() + quantity);
                    haveInCart = true;
                    break;
                }
            }
            if (!haveInCart) {
                Cartdetail cartdetail = new Cartdetail();
                cartdetail.setBookID(b);
                cartdetail.setQuantity(quantity);
                cart.getCartdetailCollection().add(cartdetail);
            }

            session.setAttribute(SessionKeys.CART, cart);
            response.sendRedirect("index/home/HomeCustomer.jsp");
        }
    }
}
