/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet.person;

import java.io.IOException;
import java.util.ArrayList;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ptit.entity.Account;
import edu.ptit.entity.Customermember;
import edu.ptit.session.AccountFacadeLocal;
import edu.ptit.session.CustomermemberFacadeLocal;
import servlet.SessionKeys;
import utility.Func;

/**
 *
 * @author Admin
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {

    private static final String TAG = "Servlet Login says: ";

    @EJB
    CustomermemberFacadeLocal customermemberDAO;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        synchronized (session) {
            System.out.println("LOGIN");
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            System.out.println("USER = " + username + "\tPASS = " + password);

            Customermember cm = customermemberDAO.login(username, password);
            if (cm != null) {
                session.setAttribute(SessionKeys.CUSTOMER.CUSOMER_LOGINED, cm);

//                ArrayList<CustomerSaleOff> cusSaleOffs;
//                cusSaleOffs = customerSaleOffDAO.checkCustomerSaleOf(cm.getId(), cm.getCardBank().getId(), Func.getCurrentDateTime());
//                if (cusSaleOffs == null) {
//                    System.out.println(TAG + "cusSaleOff = null");
//                } else {
//                    System.out.println(TAG + "cusSaleOff = " + cusSaleOffs.size());
//                }
//                session.setAttribute(SessionKeys.CUSTOMER_SALE_OFF_LIST, cusSaleOffs);
                session.setAttribute(SessionKeys.NOTICE.LOGIN_NOTICE, null);
                response.sendRedirect("index/home/HomeCustomer.jsp");
            } else {
                session.setAttribute(SessionKeys.NOTICE.LOGIN_NOTICE, "Invalid username or password!");
                response.sendRedirect("index/account/Login.jsp");
            }
        }
//        }
    }
}
