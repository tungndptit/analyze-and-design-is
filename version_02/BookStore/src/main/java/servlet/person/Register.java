package servlet.person;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author ndt
 */
@WebServlet("/Register")
public class Register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email = request.getParameter("email");    
        String username = request.getParameter("username");
        String password = request.getParameter("password");                     
        String phone = request.getParameter("phone");                     
        try {
            response.sendRedirect("index/home/HomeCustomer.jsp");
        } catch (Exception e) {
            response.sendRedirect("index/account/Register.jsp");
            // cho notice vao session
        }
    }
}
