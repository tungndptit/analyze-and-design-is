/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servlet;

/**
 *
 * @author Admin
 */
public class SessionKeys {

    public static final String SUM_COST = "sumcost";
    public static final String CUSTOMER_SALE_OFF_LIST = "cussaleoff";
    public static final String CHARGE = "charge";    

    public static final class NOTICE {

        public static final String LOGIN_NOTICE = "login_notice";
        public static final String PAYMENT_NOTICE = "payment notices!";
        public static final String HOME_NOTICE = "home notices!";
        public static final String VIEW_CART_NOTICE = "view_cart_notice";
    }

    public static final class CUSTOMER {
        public static final String CUSOMER_LOGINED = "customerMember";
        public static final String PAYMENT_ACC = "paymentacc";
        public static final String PAYMENT_PASS = "paymentpass";
    }

    public static final String CART = "cart";
    public static final String NEW_PRICES = "new prices hihi";

    public static final class BOOK {
        public static final String BOOK = "book";
        public static final String SEARCH_BOOK = "search_book";
        public static final String LIST_BOOK = "list_book";
    }

}
