/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utility;

/**
 *
 * @author Admin
 */
public class FPrice {
    public static final String VND = "vnđ";
    public static String parseToPrice(String price, String unit){        
        String res = "";
        int index = price.lastIndexOf(".");
        if (-1<index){
            price = price.substring(0,index);
        }
        int leng = price.length();
        for (int i=leng-1; i>=0; i--){
            if( i!=0 && i!=leng-1 && (leng-i-1)%3==0){
                res = "." + res;
            }
            res = price.charAt(i) + res;
        }
        return res.trim() + " " + unit;
    }

    public static String parseToPrice(float price, String unit){
        return parseToPrice(String.valueOf(price), unit);
    }
    
    public static void main(String[] args) {
        System.out.println( parseToPrice("512500.0", VND));
        System.out.println(VND);
    }
}
