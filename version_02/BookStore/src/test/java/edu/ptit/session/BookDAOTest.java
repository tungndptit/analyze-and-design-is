package edu.ptit.session;

import edu.ptit.entity.Book;

import javax.ejb.EJB;
import java.util.List;

/**
 * @author Tung NGUYEN-DUY, <tungnd.ptit@gmail.com>
 * @date 15-May-16.
 */
public class BookDAOTest {
    @EJB
    static BookFacadeLocal bookDAO;

    public static void main(String[] args) {
        List<Book> books = bookDAO.findAll();

        showList(books);
    }

    static void showList(List<Book> books){
        for (Book book : books){
            System.out.println(book.getName());
        }
    }

}
