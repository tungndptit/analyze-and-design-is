<%@page import="servlet.SessionKeys"%>
<%@page import="utility.FPrice"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="edu.ptit.entity.Book" %>
<%-- 
    Document   : HomeCustomer
    Created on : Jan 21, 2016, 4:35:22 PM
    Author     : ndt
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">
    <head>        
        <script type="text/javascript">
            function loadList() {
            <%
                if (session.getAttribute(SessionKeys.BOOK.LIST_BOOK) == null) {
            %>
                document.listform.action = "../../GetList";
                document.listform.submit();
            <%
                }
            %>
            }

            function chGate()
            {
                var e = document.getElementById("SortBook");
                var selGate = e.options[e.selectedIndex].value;
                frGate.submit();//commented out so it doesn't submit here
            }
        </script>
        <script>var selGate = "empty"</script>
    </head>
    <body onload="loadList()">
        <form name="listform" action="" method="post"></form>
        <jsp:include page="../../header.jsp" />
        <%
            synchronized (session) {
                List<Book> books = (List<Book>) ((session.getAttribute("searchBook") != null)?
                                    session.getAttribute(SessionKeys.BOOK.SEARCH_BOOK):
                                    session.getAttribute(SessionKeys.BOOK.LIST_BOOK));

        %>
        <div class="col-md-10 right">

            <div id="silde-show">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">                            
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
                </div><!-- /.carousel -->

            </div><!--end-slide-->
            <div class="tab-wapp">
                <div class="tab-content">
                    <div id="home">Sản phẩm nổi bật</div>
                    <div class="pull-right luachon">
                        <label>Sắp xếp</label>
                        <form method="GET" action="../../SortBook" name="frGate">
                            <select id="SortBook" onchange="chGate()" name = "sortbook">
                                <option  value="1">Nổi bật nhất</option>
                                <option  value="2">Giá thấp -&gt; cao</option>
                                <option  value="3">Giá cao -&gt; thấp</option>
                                <option  value="4">Mới nhất</option>
                            </select>
                        </form>
                    </div>
                </div>

            </div><!--end-luachon-->
            <div class="row">
                <div class="list-sanpham">
                    <%
                        if (books != null)
                            for (Book b : books) {
                                String price = FPrice.parseToPrice(b.getCurrentPrice(), FPrice.VND);
                                String purchase = price;
                    %>
                    <form action="../../ViewBook?idbook=<%=b.getId()%>" method="post" class="col-md-3 col-sm-6 col-xs-12">
                        <div class="thumbnail">
                            <a href="#"><img style="height:250px !important; width:200px !important;"src="../../imageBook/<%=b.getImage()%>"></a>
                            <a href="#"><p class="title"><%=b.getName()%></p></a>
                            <hr width="100%" align="center" color="#CCCCCC" />
                            <p class="giakm"><%=price%></p>
                            <% if(!purchase.equals(price)){ %>
                                <p class="giaban">Giá gốc: <%=purchase%></p>                                
                            <%} else {%>                            
                                <p class="giaban">_</p>
                            <% } %>
                            <div class="caption">
                                <p class="btn-mua"><input type="submit" class="btn btn-warning btn-mua" role="button" value="Chi tiết sản phẩm"/> </p>                                                                                                                                                                
                            </div>                                    
                        </div>
                    </form>
                    <%}%>
                    <!--end-item-->
                </div>

            </div>
            <div id="phantrang">
                <ul class="pagination">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>

                </ul>

            </div>

        </div>

    </div>
    <%}%>   
</div>
</div>
<jsp:include page="../../footer.jsp" />
<%
    String notice = (String) session.getAttribute(SessionKeys.NOTICE.HOME_NOTICE);
    session.setAttribute(SessionKeys.NOTICE.HOME_NOTICE, null);
    if (notice!=null){
        %>        
        <script>
            alert('<%=notice%>');
        </script>
        <%
    }
%>
</body>
</html>
