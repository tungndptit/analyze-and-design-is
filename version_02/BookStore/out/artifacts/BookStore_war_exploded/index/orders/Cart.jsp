<%@page import="servlet.SessionKeys" %>
<%@page import="java.util.List" %>
<%@page import="utility.FPrice" %>
<%@page import="java.util.ArrayList" %>
<%@ page import="edu.ptit.entity.Cart" %>
<%@ page import="java.util.Collection" %>
<%@ page import="edu.ptit.entity.Cartdetail" %>
<%@ page import="edu.ptit.entity.Customersaleoff" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<body>
<jsp:include page="../../header.jsp"/>
<%
    synchronized (session) {
        Cart cart = (Cart) session.getAttribute(SessionKeys.CART);
%>
<div class="col-md-10 right">

    <!--end-slide-->
    <div class="tab-wapp">
        <div class="tab-content">
            <div id="home">Sản phẩm nổi bật</div>
        </div>
        <table cellpadding="5px" cellspacing="1px" width="100%"
               style="padding-bottom:10px;background:#FFF;margin-top:10px" ;>
            <tr bgcolor="#DADADA" style="color:#000;height:30px;font: normal 13px Bitter, Myriad Pro, Verdana, serif;">
                <td align="center" style="width:4%; text-align:center;">STT</td>
                <td style=" text-align:center" align="center">Tên Sản Phẩm</td>
                <td align="center" style=" text-align:center">Giá</td>
                <td style="" align="center">size</td>
                <td style="" align="center">Số Lượng</td>
                <td style="text-align:center" align="center">Tổng Giá</td>
                <td align="center" style="width:4%; text-align:center;">Xóa</td>
            </tr>

            <%
                int numOfSet = 0;
                float saleSetBookPrice = 0;
                String s = "";
            %>
            <script>
                function priceToString(price) {
                    var total = parseInt(price + '');
                    var str = '';
                    var count = 0;
//                                    alert(total);
                    while (total > 0) {
                        str = (total % 10) + str;
//                                        alert(total+':::'+str)
                        total = parseInt((total / 10) + '');
                        count += 1;
                        if (count % 3 == 0 && total > 0) {
                            str = '.' + str;
                        }
                    }
//                                    alert('exit ' + total)
                    return str + ' vnđ';
                }

                function sumCost(id, price, firstTime) {
                    var quantity = parseInt(document.getElementById('quantity_' + id).value);
                    quantity = Math.max(quantity, 1);
                    quantity = Math.min(quantity, 100);
                    document.getElementById('quantity_' + id).value = quantity;
                    var total = quantity * parseFloat(price);
                    document.getElementById('sum_cost_' + id).innerHTML = priceToString(total);
                    if (firstTime == false) {
//                                        alert('Second time');
                        <%

                        %>
                    }
                }
            </script>
            <%
                Collection<Cartdetail> cartdetails = cart.getCartdetailCollection();
                if (cartdetails != null) {
                    int index = -1;
                    for (Cartdetail cartdetail : cartdetails) {
                        index++;
            %>
            <tr style="text-align: center;border:1px solid #ddd;">
                <td width="3%" align="center" style=" text-align:center;"><%=(index + 1)%>
                </td>
                <td width="29%">
                    <div class="box-basket1">
                        <div class="box-basket1-img">
                            <img src="../../imageBook/<%=cartdetail.getBookID().getImage()%>" width="60" height="90"
                                 style="float:left; margin-bottom:10px" alt="<%=cartdetail.getBookID().getName()%>">
                        </div>
                        <div class="box-basket1-name"><h2
                                style="font-size:12px;padding-top:30px;"><%=cartdetail.getBookID().getName()%>
                        </h2></div>
                        <div class="box-basket1-mota"></div>
                    </div>
                </td>
                <td width="17%" align="center" style="text-align:center;">
                    <script> document.write(<%=FPrice.parseToPrice(cartdetail.getBookID().getCurrentPrice(), FPrice.VND)%>)</script>
                </td>
                <td width="5%"></td>
                <td width="15%" align="center" style="text-align:center;">
                    <input id="quantity_<%=String.valueOf(index)%>" type="number" min="1" max="100"
                           onChange="sumCost(<%=index%>,<%=(int)cartdetail.getBookID().getCurrentPrice()%>, false)"
                           value="<%=cartdetail.getQuantity()%>" maxlength="3" size="2"
                           style="text-align:center; border:1px solid #F0F0F0;margin-top:10px;width:50%;"/>
                    &nbsp;
                </td>
                <td id="sum_cost_<%=String.valueOf(index)%>" width="18%" align="center" style="text-align:center;"></td>
                <script>
                    sumCost(<%=index%>, <%=cartdetail.getBookID().getCurrentPrice()%>, true);
                </script>
                <td align="center" style="text-align:center;">
                    <a href="../../DeleteBook?id=<%=index%>"><img src="../../img/delete.gif" border="0"/></a></td>
            </tr>
            <%
                    }
                }
            %>
            <tr>
                <td colspan="7" align="right" style="padding-top:10px; padding-left:10px;padding-bottom:20px;">
                    <% if (session.getAttribute(SessionKeys.CUSTOMER.PAYMENT_ACC) == null) {%>
                    <a href="../account/payment.jsp" class="button" style="padding:5px 10px;">
                        <button type="button" class="btn btn-primary btn-dangky" style="margin-top:20px;">
                            Thanh Toán
                        </button>
                    </a></td>
                <%} else { %>
                <form action="../../Payment" method="POST">
                    <button type="submit" class="btn btn-primary btn-login">Thanh Toán</button>
                </form>
                <% }%>


            </tr>

        </table>
        <%
            Float sumCost = (Float) session.getAttribute(SessionKeys.SUM_COST);
            Float charge = (Float) session.getAttribute(SessionKeys.CHARGE);
        %>

        <p>Tổng tiền: <%=FPrice.parseToPrice(String.valueOf(sumCost), FPrice.VND)%>
        </p>
        <p>Số tiền phải trả: <%=FPrice.parseToPrice(String.valueOf(charge), FPrice.VND)%>
        </p>
        <%
            ArrayList<Customersaleoff> cusSaleOffs = (ArrayList<Customersaleoff>) session.getAttribute(SessionKeys.CUSTOMER_SALE_OFF_LIST);
            if (cusSaleOffs != null) { %>
        <p> Danh mục khuyến mãi</p>
        <% for (Customersaleoff cso : cusSaleOffs) {
            String name = "Khuyến mại cho khách hàng: ";
            if (cso.getBankID() == null) {
                name = "Khuyến mại theo ngân hàng thanh toán: ";
            }
            String amount = "";
            if (cso.getPrice() != 0) {
                amount = FPrice.parseToPrice(String.valueOf(cso.getPrice()), FPrice.VND);
            } else {
                amount = String.valueOf(cso.getPercent()) + "%";
                float sub = cso.getPercent() * sumCost / 100;
                amount += " (" + FPrice.parseToPrice(String.valueOf(sub), FPrice.VND) + ")";
            } %>
        <p><%=name + amount%>
        </p>
        <% }
        } %>
    </div>

</div>
<%}%>
</div>
<!--end-right-->
</div>
<jsp:include page="../../footer.jsp"/>
<%
    String notice = (String) session.getAttribute(SessionKeys.NOTICE.VIEW_CART_NOTICE);
    session.setAttribute(SessionKeys.NOTICE.VIEW_CART_NOTICE, null);
    if (notice != null) {
%>
<script>
    alert('<%=notice%>');
</script>
<%
    }
%>
</body>
</html>
