<%@page import="servlet.SessionKeys"%>
<%@page import="utility.FPrice"%>
<%@page import="java.util.ArrayList"%>
<%@ page import="edu.ptit.entity.Cart" %>
<%@ page import="edu.ptit.entity.Book" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
    
    <body>
        <jsp:include page="../../header.jsp" />
        <%
            synchronized (session) {
                Book book = (Book) session.getAttribute(SessionKeys.BOOK.BOOK);
                String price = FPrice.parseToPrice(book.getCurrentPrice(), FPrice.VND);
//                String purchase = (!book.getOriginalPrice().equals(book.getSalePrice()))?
//                    FPrice.parseToPrice(book.getOriginalPrice(), FPrice.VND):price;
                String purchase = price;

        %>        
                <div class="col-md-10 right" style="height: 650px">

                    <div class="col-md-5">
                        <img src="../../imageBook/<%=book.getImage()%>" style="width:300px;height:450px;
                             margin-left: -10px;
                             background: #fff;
                             padding: 5px;border:1px solid #CCC;"> 
                    </div>
                    <div class="col-md-7">
                        <h5 style="font-size:25px;"><%=book.getName()%></h5>
                        <hr width="100%" color="#DCDCDC">
                        <p style="padding-top:10px;">Khuyến mãi: <span style="color:#FF4400;font-weight:bold;font-size:15px;"><%=price%></span></p>
                        <p style="text-decoration:line-through;">Giá bán: <%=purchase%></p>
                        <p style="background:#FFEBAF;padding:10px 10px;"><%=book.getDescription()%></p>
                        <p style="background:#FFEBAF;padding:10px 10px;">
                            Author: <%=book.getAuthorName()%><br>
                            Category: <%=book.getCategoryID().getName()%><br>
                            Publisher: <%=book.getPublisherID().getName()%><br>
                            Publish Year: <%=book.getPublisherYear()%> <br>
                        </p>
                        <script>
                            function changeBookNumber(v) {
                                var tagNumber = document.getElementById("num_book");
                                var curr = parseInt(tagNumber.getAttribute("value"));
                                curr = curr + v;
                                if (curr < 1) {
                                    curr = 1;
                                }
                                if (curr > 100) {
                                    curr = 100;
                                }
                                tagNumber.setAttribute("value", curr);
                            }
                            function addToCart() {
                                document.submitAddToCart.action = "../../AddToCart";
                                document.submitAddToCart.method = "post";
                                var quantity = document.getElementById("num_book").getAttribute("value");
                                document.getElementById("bookQuantity").setAttribute("value", quantity);
                                document.submitAddToCart.submit();
                            }
                        </script>
                        <div class="add-to-cart-footer" style="width:100%;">
                            <div class="btn-add" style="width:60%;">
                                <button onclick="changeBookNumber(-1)">-</button>
                                <input id="num_book"type="text" class="add" style="width:40px; background: #ccc;" value="1"/>
                                <button onclick="changeBookNumber(1)">+</button>
                            </div>
                            <div class="form-add-to-cart" style="width:50%; float:right;    margin-top: -23px;">
                                <form name="submitAddToCart">                            
                                    <input type="hidden" name="idOrder" value="<%=book.getId()%>"/>
                                    <input type="hidden" name="bookQuantities" id="bookQuantity" value="1"/>
                                </form>
                                <p class="btn-mua">
                                    <input type="submit" class="btn btn-warning btn-mua" role="button" value="Thêm vào giỏ hàng" onClick="addToCart()"/> 
                                </p> 
                            </div>
                        </div>
                    </div><!--end-col-md-10-->

                </div>
                <%}%>
            </div>
            <!--end-right-->
        </div><!--end-container-->    

        <jsp:include page="../../footer.jsp" />
    </body>
</html>
