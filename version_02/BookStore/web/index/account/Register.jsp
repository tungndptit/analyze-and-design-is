<%@page import="edu.authentication.service.ViewCapcha"%>
<%@page import="entity.person.CustomerMember"%>
<%@page import="entity.orders.BookOrder"%>
<%@page import="entity.orders.BookOrder"%>
<%@page import="entity.orders.Cart"%>
<%@ page import="entity.book.Book" %>
<%@ page import="java.util.ArrayList" %>
<%-- 
    Document   : HomeCustomer
    Created on : Jan 21, 2016, 4:35:22 PM
    Author     : ndt
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">

    <body>
        <jsp:include page="../../header.jsp" />
        <div class="col-md-10 right">

            <!--end-slide-->
            <div class="tab-wapp">
                <div class="tab-content">
                    <div id="home">Tạo tài khoản mới</div>
                </div>
                <div class="form-login">
                    <div class="col-md-12 form-dangky">
                        <p style="font-size:12px; margin-top: 10px;">Đăng ký tài khoản để nhận ưu đãi từ chúng tôi.</p>
                        <form action="../../Register" method="POST" onsubmit="return checkForm()">
                            <div class="form-group">
                                <label>Tên đăng nhập:*</label>
                                <input name="username" id="username" type="text" class="form-control dang-ky" placeholder="User-name">
                                <p id="checkUser" style="color: red;"/>
                            </div>
                            <div class="form-group">
                                <label>Email:*</label>
                                <input name="email" id="email" type="email" class="form-control dang-ky" placeholder="Email"> 
                                <p id="checkEmail" style="color: red;"/>
                            </div>
                            <div class="form-group">
                                <label>Mật khẩu:*</label>
                                <input type="password" id="password" name="password" class="form-control dang-ky" placeholder="Password">
                                <p id="checkPass" style="color: red;"/>
                            </div>
                            <div class="form-group">
                                <label>Nhập lại mật khẩu:*</label>
                                <input type="password"  id="repassword" class="form-control dang-ky" placeholder="Re-password"> 
                                <p id="checkRePass" style="color: red;"/>
                            </div>
                            <div class="form-group">
                                <label>Điện thoại:*</label>
                                <input type="text" id="phone" name="phone" class="form-control dang-ky" placeholder="Phone number">
                            </div>                                    
                            <div class="form-group list-ma-kt">
                                <label>Mã kiểm tra:*</label>
                                <input type="text" name="code" class="form-control ma-kiem-tra" ><a href="#"><img src="../../img/load.png"class="img-load"></a>
                                <%
                                    String imageAuthen = null;
                                    try {
                                        imageAuthen = new ViewCapcha().getStringByteArr();
                                        if(imageAuthen!=null){
                                            %>
                                            <img src="data:image/jpeg;base64,<%=imageAuthen%>
                                            <%
                                        }
                                    } catch(Exception ex){                                        
                                    }                                    
                                %>                                
                            </div>                                    
                            <input type="submit" class="btn btn-primary btn-dangky" value="Đăng Ký"/>
                        </form>
                        <script>
                            function checkForm() {
                                $("#checkUser").html("");
                                $("#checkPass").html("");
                                $("#checkRePass").html("");
                                $("#checkEmail").html("");
                                if ($("#username").val() == "") {
                                    $("#checkUser").html(" Tài khoản không được để trống");
                                    return false;
                                } else {
                                    var reg = new RegExp(/^(?=.{6,45}$)[a-zA-Z0-9_-]/g);
                                    var result = $("#username").val().search(reg);
                                    if (result == -1) {
                                        $("#checkUser").html("Chỉ cho phép các ký tự A-Z,a-z,0-9,_,- và từ 6 đến 45 ký tự");
                                        return false;
                                    }
                                }
                                if ($("#email").val() == "") {
                                    $("#checkEmail").html(" Email ko được để trống");
                                    return false;
                                }
                                if ($("#password").val() == "") {
                                    $("#checkPass").html("Mật khẩu không được để trống");
                                    return false;
                                }
                                if ($("#password").val() != $("#repassword").val()) {
                                    $("#checkRePass").html("Mật khẩu không khớp!");
                                    return false;
                                }
                                return true;
                            }
                        </script>

                    </div>
                </div><!--end-from-->


            </div>

        </div>
        <!--end-right-->
    </div>
    <!--end-right-->
</div><!--end-container-->
</div>
<jsp:include page="../../footer.jsp" />
</body>
</html>
