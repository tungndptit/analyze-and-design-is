<%@page import="servlet.SessionKeys"%>
<%@ page import="java.util.ArrayList" %>
<%-- 
    Document   : HomeCustomer
    Created on : Jan 21, 2016, 4:35:22 PM
    Author     : ndt
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">

    <body>
        <jsp:include page="../../header.jsp" />
        <div class="col-md-10 right">

            <!--end-slide-->
            <div class="tab-wapp">
                <div class="tab-content">
                    <div id="home">Khách hàng đã đăng ký</div>
                </div>

                <div class="form-login">
                    <div class="col-md-6 login-left">                                
                        <p style="font-size:12px; margin-top: 10px;">Nếu bạn có một tài khoản với chúng tôi, xin vui lòng đăng nhập.</p>
                        <form action="../../Login" method="POST">
                            <div class="form-group">
                                <label>Tên đăng nhập:*</label>
                                <input name="username" type="text" class="form-control" placeholder="User-name or email"style>
                            </div>
                            <div class="form-group">
                                <label>Mật khẩu:*</label>
                                <input name="password" type="password" class="form-control" placeholder="Password"> <a href="#"><p style="margin-top:10px;">Quên mật khẩu?</p></a>
                            </div>
                            <div class="checkbox">
                                <label><input name="remember" type="checkbox" value="true"> Ghi Nhớ</label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-login">Đăng Nhập</button>
                        </form>




                    </div><!--end-from-left-->
                    <div class="col-md-6 form-right">
                        <h3>Khách hàng mới</h3>
                        <p style="background:#FAEBD7;padding : 5px 10px 5px 10px;margin-top:20px;">Bằng cách tạo một tài khoản với cửa hàng của chúng tôi, bạn sẽ có quy trình kiểm tra nhanh hơn, ưu đãi mua hàng với giá sỉ, xem và theo dõi đơn đặt hàng của bạn trong tài khoản của bạn và nhiều hơn nữa.</p>
                        <a href="../account/Register.jsp">
                            <button type="button" class="btn btn-primary btn-dangky" style="margin-top:20px;" >
                                Tạo một tài khoản
                            </button></a>


                    </div>

                </div>

            </div>

        </div>
        <!--end-right-->
    </div>
    <!--end-right-->
</div><!--end-container-->
</div>
<jsp:include page="../../footer.jsp" />
<%
    String notice = (String) session.getAttribute(SessionKeys.NOTICE.LOGIN_NOTICE);    
    session.setAttribute(SessionKeys.NOTICE.LOGIN_NOTICE, null);
    if (notice!=null){
        %>        
        <script>
            alert('<%=notice%>');
        </script>
        <%
    }
%>
</body>
</html>
