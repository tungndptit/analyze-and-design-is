<%-- 
    Document   : header
    Created on : Apr 5, 2016, 2:30:53 PM
    Author     : Admin
--%>

<%@page import="servlet.SessionKeys" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@page import="utility.FPrice" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Collection" %>
<%@ page import="edu.ptit.entity.*" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Book Store</title>

    <!-- Bootstrap -->

    <link href="../../css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../../css/style.css">
    <script src="../../js/jquery.js"></script>
    <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
</head>
<body>
<%
    Customermember customerMember = (Customermember) session.getAttribute(SessionKeys.CUSTOMER.CUSOMER_LOGINED);
%>
<div id="header">
    <div class="container">
        <div class="col-md-4">
            <div class="row">
                <p class="hotline"><span>Hotline: </span>0962 096 358 - 0948 555 806</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="row">
                <form class="input-group search-bar" action="../../SearchBookByName" method="GET">
                    <input name="searchBook" type="text" class="form-control" placeholder="Nhập từ khóa">
                            <span class="input-group-btn">
                                <button class="btn btn-default btn-search" type="submit"> <span
                                        class="glyphicon glyphicon-search icon-search"></span></button>
                            </span>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            <div class="pull-right login">
                <% if (customerMember != null) {
                %>
                <span>Xin chào,<a href="#"> <%=customerMember.getPersonID().getFullnameID().getFullName()%> |</a></span>
                <a href="../../Logout"> Đăng xuất</a>
                <%} else {%>
                <a href="../account/Login.jsp">Đăng nhập |</a>
                <a href="../account/Register.jsp">Đăng ký </a>
                <%}%>
            </div>

        </div>
    </div><!--end-container-top-->


    <div class="container">
        <hr class="top-bootom" color="#000000">
        <div class="col-md-2 col-xs-12 col-sm-12">
            <div class="row">
                <img src="../../img/logo.jpg" alt="" id="logo"/>
            </div>
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12">
            <div class="row">
                <div class="navbar navbar-inverse menu-ngang" role="navigation">
                    <div class="">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse"
                                    data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav nav-home">
                                <!--                                            <li><form action="../../GetList" method="POST"><input type="submit" value="Trang chu"></input></form>
                                                                            </li>-->
                                <li><a href="../../HomePage" title="">Trang chủ</a>
                                </li>
                                <li><a href="" title="">Bán chạy</a>
                                </li>
                                <li><a href="" title="">Tuyển tập</a>
                                </li>

                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-2 col-xs-12 col-sm-12">
            <div class="row row-cart">
                <a href="../../ViewCart"><span class="glyphicon glyphicon-shopping-cart cart"> Giỏ hàng (0)</span></a>
                <a href="../../SaleBook"><img src="../../img/giohang.png" class="img-cart"></a>
                <div class="media list-cart">
                    <%
                        Cart cart = (Cart) session.getAttribute("cart");
                        if (cart == null) {
                            cart = new Cart();
                        }
                        Collection<Cartdetail> cartdetails = cart.getCartdetailCollection();
                        if (cartdetails != null) {
                            for (Cartdetail cartdetail : cartdetails){
                                Book book = cartdetail.getBookID();
                    %>
                    ;
                    <div class="cart-item">
                        <a class="pull-left" href="#">
                            <img class="media-object" src="../../imageBook/<%=book.getImage()%>"
                                 style="width:70px;height: 115px">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading"><%=book.getName()%>
                            </h4>
                            <p>Số lượng: <%=cartdetail.getQuantity()%> | Giá bán:
                                <%--<%=FPrice.parseToPrice(((ArrayList<Price>)book.getPriceCollection()).get(0), FPrice.VND)%>--%>
                            </p>

                        </div>
                    </div>
                    <%
                            }
                        }
                        if (cartdetails == null) {
                    %>
                    <div class="cart-item">
                        <b><p>Bạn chưa có món hàng nào trong giỏ.</p></b>
                    </div>
                    <%
                        }
                    %>
                    <div class="btn-cart" style="margin-top: 20px">
                    </div>

                </div><!--end-media-list-cart-->
            </div>
        </div>

    </div><!--end-container--boot-->
</div>
<!--End-header-->
<div id="wapper">
    <div class="container">
        <div class="col-md-2 left">
            <h5 style="color :#6B6972;font-weight: bold;">Danh mục sản phẩm</h5>
            <ul class="list-unstyled">
                <li><a href="" title="">SP 1</a>
                </li>
                <li><a href="" title="">SP 2</a>
                </li>
                <li><a href="" title="">SP 3</a>
                </li>
                <li><a href="" title="">SP 4</a>
                </li>
            </ul>
        </div>

</body>
</html>
