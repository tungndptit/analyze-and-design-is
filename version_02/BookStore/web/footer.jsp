<%-- 
    Document   : footer
    Created on : Apr 5, 2016, 2:36:20 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>   
    <body>
        <!--end-wapper-->
        <div id="footer">
            <div class="container">
                <div class="row footer">
                    <div class="col-md-3">
                        <h4 class="title-footer">Hỗ trợ khách hàng</h4>
                        <ul class="list-unstyled list-hotro">
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"> Hướng dẫn đăng ký tài khoản</span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"> Quy cách đóng gói hàng</span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"> Câu hỏi thường gặp</span></a></li>
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"> Chính sách vận chuyển</span></a></li>
                        </ul>

                    </div>
                    <div class="col-md-3">
                        <h4 class="title-footer">THÔNG TIN VỀ VIETFAS.COM</h4>
                        <ul class="list-unstyled list-hotro">
                            <li><a href="#"><span class="glyphicon glyphicon-chevron-right"> Khai trương bán hàng online</span></a></li>

                        </ul>
                    </div>
                    <div class="col-md-3 pay">
                        <h4 class="title-footer">Hình thức thanh toán</h4>
                        <img src="../../img/pay.png">
                        <h4 class="title-footer">Chứng nhận của bộ công thương</h4>
                        <img src="../../img/chungchi.png">
                    </div>
                    <div class="col-md-3 ketnoi">
                        <h4 class="title-footer">Kết nối với chúng tôi</h4>
                        <ul>
                            <li><a href="#"><img src="../../img/fb.png"> Facebook</a></li>
                            <li><a href="#"><img src="../../img/ytb.png"> Youtube</a></li>
                            <li><a href="#"><img src="../../img/gg.png"> Google +</a></li>
                            <li><a href="#"><img src="../../img/tt.png"> Twitter</a></li>
                        </ul>
                    </div>
                    <hr class="top-bootom" width="100%" align="center" color="#205081" />
                    <h3 style="font-size:15px;text-align:center;color:#fff;font-weight:bold;">SHOP GIÀY NỮ ĐẸP - VIETFAS WOMEN'S SHOES</h3>
                    <p style="font-size:12px;text-align:center;color:#fff">Địa chỉ: Số 59, Ngõ 25, phố Vũ Ngọc Phan, Đống Đa, Hà Nội</p>
                    <p style="font-size:12px;text-align:center;color:#fff">Hotline: 0962 096 358 - 0948 555 806 | Website: www.vietfas.com - Email: info@vietfas.com</p>
                </div><!--end-row-->
            </div><!--end-container-->
        </div><!--end-footer-->    
    </body>
</html>
