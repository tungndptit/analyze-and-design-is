/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "addressshipping")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Addressshipping.findAll", query = "SELECT a FROM Addressshipping a"),
    @NamedQuery(name = "Addressshipping.findById", query = "SELECT a FROM Addressshipping a WHERE a.id = :id")})
public class Addressshipping implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @JoinColumn(name = "AddressID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Address addressID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "addressShippingID")
    private Collection<Shippinginfor> shippinginforCollection;

    public Addressshipping() {
    }

    public Addressshipping(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Address getAddressID() {
        return addressID;
    }

    public void setAddressID(Address addressID) {
        this.addressID = addressID;
    }

    @XmlTransient
    public Collection<Shippinginfor> getShippinginforCollection() {
        return shippinginforCollection;
    }

    public void setShippinginforCollection(Collection<Shippinginfor> shippinginforCollection) {
        this.shippinginforCollection = shippinginforCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Addressshipping)) {
            return false;
        }
        Addressshipping other = (Addressshipping) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Addressshipping[ id=" + id + " ]";
    }
    
}
