/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "billdetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Billdetail.findAll", query = "SELECT b FROM Billdetail b"),
    @NamedQuery(name = "Billdetail.findById", query = "SELECT b FROM Billdetail b WHERE b.id = :id"),
    @NamedQuery(name = "Billdetail.findByQuantity", query = "SELECT b FROM Billdetail b WHERE b.quantity = :quantity"),
    @NamedQuery(name = "Billdetail.findByPrice", query = "SELECT b FROM Billdetail b WHERE b.price = :price")})
public class Billdetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Quantity")
    private int quantity;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Price")
    private float price;
    @JoinColumn(name = "PriceID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Price priceID;
    @JoinColumn(name = "BillID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Bill billID;
    @JoinColumn(name = "CategorySaleOffID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Categorysaleoff categorySaleOffID;

    public Billdetail() {
    }

    public Billdetail(Integer id) {
        this.id = id;
    }

    public Billdetail(Integer id, int quantity, float price) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public Price getPriceID() {
        return priceID;
    }

    public void setPriceID(Price priceID) {
        this.priceID = priceID;
    }

    public Bill getBillID() {
        return billID;
    }

    public void setBillID(Bill billID) {
        this.billID = billID;
    }

    public Categorysaleoff getCategorySaleOffID() {
        return categorySaleOffID;
    }

    public void setCategorySaleOffID(Categorysaleoff categorySaleOffID) {
        this.categorySaleOffID = categorySaleOffID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Billdetail)) {
            return false;
        }
        Billdetail other = (Billdetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Billdetail[ id=" + id + " ]";
    }
    
}
