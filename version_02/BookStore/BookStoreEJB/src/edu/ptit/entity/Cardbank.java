/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "cardbank")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cardbank.findAll", query = "SELECT c FROM Cardbank c"),
    @NamedQuery(name = "Cardbank.findById", query = "SELECT c FROM Cardbank c WHERE c.id = :id"),
    @NamedQuery(name = "Cardbank.findByCardType", query = "SELECT c FROM Cardbank c WHERE c.cardType = :cardType"),
    @NamedQuery(name = "Cardbank.findByNameOwner", query = "SELECT c FROM Cardbank c WHERE c.nameOwner = :nameOwner"),
    @NamedQuery(name = "Cardbank.findByCardNumber", query = "SELECT c FROM Cardbank c WHERE c.cardNumber = :cardNumber")})
public class Cardbank implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Size(max = 255)
    @Column(name = "CardType")
    private String cardType;
    @Size(max = 255)
    @Column(name = "NameOwner")
    private String nameOwner;
    @Size(max = 255)
    @Column(name = "CardNumber")
    private String cardNumber;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cardBankID")
    private Collection<Bankaccount> bankaccountCollection;

    public Cardbank() {
    }

    public Cardbank(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getNameOwner() {
        return nameOwner;
    }

    public void setNameOwner(String nameOwner) {
        this.nameOwner = nameOwner;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    @XmlTransient
    public Collection<Bankaccount> getBankaccountCollection() {
        return bankaccountCollection;
    }

    public void setBankaccountCollection(Collection<Bankaccount> bankaccountCollection) {
        this.bankaccountCollection = bankaccountCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cardbank)) {
            return false;
        }
        Cardbank other = (Cardbank) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Cardbank[ id=" + id + " ]";
    }
    
}
