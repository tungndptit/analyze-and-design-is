/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "shippinginfor")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Shippinginfor.findAll", query = "SELECT s FROM Shippinginfor s"),
    @NamedQuery(name = "Shippinginfor.findById", query = "SELECT s FROM Shippinginfor s WHERE s.id = :id")})
public class Shippinginfor implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "shippingInforID")
    private Collection<Orders> ordersCollection;
    @JoinColumn(name = "CustomerMemberID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Customermember customerMemberID;
    @JoinColumn(name = "AddressShippingID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Addressshipping addressShippingID;

    public Shippinginfor() {
    }

    public Shippinginfor(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @XmlTransient
    public Collection<Orders> getOrdersCollection() {
        return ordersCollection;
    }

    public void setOrdersCollection(Collection<Orders> ordersCollection) {
        this.ordersCollection = ordersCollection;
    }

    public Customermember getCustomerMemberID() {
        return customerMemberID;
    }

    public void setCustomerMemberID(Customermember customerMemberID) {
        this.customerMemberID = customerMemberID;
    }

    public Addressshipping getAddressShippingID() {
        return addressShippingID;
    }

    public void setAddressShippingID(Addressshipping addressShippingID) {
        this.addressShippingID = addressShippingID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Shippinginfor)) {
            return false;
        }
        Shippinginfor other = (Shippinginfor) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Shippinginfor[ id=" + id + " ]";
    }
    
}
