/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "categorysaleoff")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Categorysaleoff.findAll", query = "SELECT c FROM Categorysaleoff c"),
    @NamedQuery(name = "Categorysaleoff.findById", query = "SELECT c FROM Categorysaleoff c WHERE c.id = :id"),
    @NamedQuery(name = "Categorysaleoff.findByStartDate", query = "SELECT c FROM Categorysaleoff c WHERE c.startDate = :startDate"),
    @NamedQuery(name = "Categorysaleoff.findByEndDate", query = "SELECT c FROM Categorysaleoff c WHERE c.endDate = :endDate"),
    @NamedQuery(name = "Categorysaleoff.findByPercent", query = "SELECT c FROM Categorysaleoff c WHERE c.percent = :percent"),
    @NamedQuery(name = "Categorysaleoff.findByMaxPrice", query = "SELECT c FROM Categorysaleoff c WHERE c.maxPrice = :maxPrice")})
public class Categorysaleoff implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "StartDate")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "EndDate")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Percent")
    private float percent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxPrice")
    private float maxPrice;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categorySaleOffID")
    private Collection<Billdetail> billdetailCollection;
    @JoinColumn(name = "CategoryID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Category categoryID;

    public Categorysaleoff() {
    }

    public Categorysaleoff(Integer id) {
        this.id = id;
    }

    public Categorysaleoff(Integer id, float percent, float maxPrice) {
        this.id = id;
        this.percent = percent;
        this.maxPrice = maxPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public float getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(float maxPrice) {
        this.maxPrice = maxPrice;
    }

    @XmlTransient
    public Collection<Billdetail> getBilldetailCollection() {
        return billdetailCollection;
    }

    public void setBilldetailCollection(Collection<Billdetail> billdetailCollection) {
        this.billdetailCollection = billdetailCollection;
    }

    public Category getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(Category categoryID) {
        this.categoryID = categoryID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Categorysaleoff)) {
            return false;
        }
        Categorysaleoff other = (Categorysaleoff) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Categorysaleoff[ id=" + id + " ]";
    }
    
}
