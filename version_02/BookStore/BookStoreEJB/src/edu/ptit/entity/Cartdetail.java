/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "cartdetail")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cartdetail.findAll", query = "SELECT c FROM Cartdetail c"),
    @NamedQuery(name = "Cartdetail.findById", query = "SELECT c FROM Cartdetail c WHERE c.id = :id"),
    @NamedQuery(name = "Cartdetail.findByQuantity", query = "SELECT c FROM Cartdetail c WHERE c.quantity = :quantity")})
public class Cartdetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "quantity")
    private Integer quantity;
    @JoinColumn(name = "bookID", referencedColumnName = "ID")
    @ManyToOne
    private Book bookID;
    @JoinColumn(name = "cartID", referencedColumnName = "ID")
    @ManyToOne
    private Cart cartID;

    public Cartdetail() {
    }

    public Cartdetail(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Book getBookID() {
        return bookID;
    }

    public void setBookID(Book bookID) {
        this.bookID = bookID;
    }

    public Cart getCartID() {
        return cartID;
    }

    public void setCartID(Cart cartID) {
        this.cartID = cartID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cartdetail)) {
            return false;
        }
        Cartdetail other = (Cartdetail) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Cartdetail[ id=" + id + " ]";
    }
    
}
