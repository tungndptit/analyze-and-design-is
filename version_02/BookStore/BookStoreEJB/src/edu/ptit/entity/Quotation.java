/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "quotation")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Quotation.findAll", query = "SELECT q FROM Quotation q"),
    @NamedQuery(name = "Quotation.findById", query = "SELECT q FROM Quotation q WHERE q.id = :id"),
    @NamedQuery(name = "Quotation.findByStartDate", query = "SELECT q FROM Quotation q WHERE q.startDate = :startDate"),
    @NamedQuery(name = "Quotation.findByEndDate", query = "SELECT q FROM Quotation q WHERE q.endDate = :endDate"),
    @NamedQuery(name = "Quotation.findByMaxQuantity", query = "SELECT q FROM Quotation q WHERE q.maxQuantity = :maxQuantity"),
    @NamedQuery(name = "Quotation.findByStatus", query = "SELECT q FROM Quotation q WHERE q.status = :status")})
public class Quotation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Column(name = "StartDate")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "EndDate")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxQuantity")
    private int maxQuantity;
    @Size(max = 255)
    @Column(name = "Status")
    private String status;
    @JoinColumn(name = "MessageID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Message messageID;
    @JoinColumn(name = "BookID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Book bookID;
    @JoinColumn(name = "ProviderID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Provider providerID;

    public Quotation() {
    }

    public Quotation(Integer id) {
        this.id = id;
    }

    public Quotation(Integer id, int maxQuantity) {
        this.id = id;
        this.maxQuantity = maxQuantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(int maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Message getMessageID() {
        return messageID;
    }

    public void setMessageID(Message messageID) {
        this.messageID = messageID;
    }

    public Book getBookID() {
        return bookID;
    }

    public void setBookID(Book bookID) {
        this.bookID = bookID;
    }

    public Provider getProviderID() {
        return providerID;
    }

    public void setProviderID(Provider providerID) {
        this.providerID = providerID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Quotation)) {
            return false;
        }
        Quotation other = (Quotation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Quotation[ id=" + id + " ]";
    }
    
}
