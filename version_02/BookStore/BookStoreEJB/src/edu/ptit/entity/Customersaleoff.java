/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "customersaleoff")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customersaleoff.findAll", query = "SELECT c FROM Customersaleoff c"),
    @NamedQuery(name = "Customersaleoff.findById", query = "SELECT c FROM Customersaleoff c WHERE c.id = :id"),
    @NamedQuery(name = "Customersaleoff.findByPrice", query = "SELECT c FROM Customersaleoff c WHERE c.price = :price"),
    @NamedQuery(name = "Customersaleoff.findByPercent", query = "SELECT c FROM Customersaleoff c WHERE c.percent = :percent"),
    @NamedQuery(name = "Customersaleoff.findByMaxPrice", query = "SELECT c FROM Customersaleoff c WHERE c.maxPrice = :maxPrice"),
    @NamedQuery(name = "Customersaleoff.findByStartDate", query = "SELECT c FROM Customersaleoff c WHERE c.startDate = :startDate"),
    @NamedQuery(name = "Customersaleoff.findByEndDate", query = "SELECT c FROM Customersaleoff c WHERE c.endDate = :endDate")})
public class Customersaleoff implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Price")
    private float price;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Percent")
    private float percent;
    @Basic(optional = false)
    @NotNull
    @Column(name = "MaxPrice")
    private float maxPrice;
    @Column(name = "StartDate")
    @Temporal(TemporalType.DATE)
    private Date startDate;
    @Column(name = "EndDate")
    @Temporal(TemporalType.DATE)
    private Date endDate;
    @JoinColumn(name = "CustomerID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Customermember customerID;
    @JoinColumn(name = "BankID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Bank bankID;

    public Customersaleoff() {
    }

    public Customersaleoff(Integer id) {
        this.id = id;
    }

    public Customersaleoff(Integer id, float price, float percent, float maxPrice) {
        this.id = id;
        this.price = price;
        this.percent = percent;
        this.maxPrice = maxPrice;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getPercent() {
        return percent;
    }

    public void setPercent(float percent) {
        this.percent = percent;
    }

    public float getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(float maxPrice) {
        this.maxPrice = maxPrice;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Customermember getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Customermember customerID) {
        this.customerID = customerID;
    }

    public Bank getBankID() {
        return bankID;
    }

    public void setBankID(Bank bankID) {
        this.bankID = bankID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customersaleoff)) {
            return false;
        }
        Customersaleoff other = (Customersaleoff) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Customersaleoff[ id=" + id + " ]";
    }
    
}
