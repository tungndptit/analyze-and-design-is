/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NDT
 */
@Entity
@Table(name = "customermember")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Customermember.findAll", query = "SELECT c FROM Customermember c"),
    @NamedQuery(name = "Customermember.findById", query = "SELECT c FROM Customermember c WHERE c.id = :id"),
    @NamedQuery(name = "Customermember.findByEmail", query = "SELECT c FROM Customermember c WHERE c.email = :email"),
    @NamedQuery(name = "Customermember.findByPhone", query = "SELECT c FROM Customermember c WHERE c.phone = :phone")})
public class Customermember implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID")
    private Integer id;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "Email")
    private String email;
    // @Pattern(regexp="^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$", message="Invalid phone/fax format, should be as xxx-xxx-xxxx")//if the field contains phone or fax number consider using this annotation to enforce field validation
    @Size(max = 255)
    @Column(name = "Phone")
    private String phone;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerID")
    private Collection<Bankaccount> bankaccountCollection;
    @JoinColumn(name = "AccountID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Account accountID;
    @JoinColumn(name = "PersonID", referencedColumnName = "ID")
    @ManyToOne(optional = false)
    private Person personID;
    @OneToMany(mappedBy = "customerID")
    private Collection<Cart> cartCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerID")
    private Collection<Customersaleoff> customersaleoffCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "customerMemberID")
    private Collection<Shippinginfor> shippinginforCollection;

    public Customermember() {
    }

    public Customermember(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @XmlTransient
    public Collection<Bankaccount> getBankaccountCollection() {
        return bankaccountCollection;
    }

    public void setBankaccountCollection(Collection<Bankaccount> bankaccountCollection) {
        this.bankaccountCollection = bankaccountCollection;
    }

    public Account getAccountID() {
        return accountID;
    }

    public void setAccountID(Account accountID) {
        this.accountID = accountID;
    }

    public Person getPersonID() {
        return personID;
    }

    public void setPersonID(Person personID) {
        this.personID = personID;
    }

    @XmlTransient
    public Collection<Cart> getCartCollection() {
        return cartCollection;
    }

    public void setCartCollection(Collection<Cart> cartCollection) {
        this.cartCollection = cartCollection;
    }

    @XmlTransient
    public Collection<Customersaleoff> getCustomersaleoffCollection() {
        return customersaleoffCollection;
    }

    public void setCustomersaleoffCollection(Collection<Customersaleoff> customersaleoffCollection) {
        this.customersaleoffCollection = customersaleoffCollection;
    }

    @XmlTransient
    public Collection<Shippinginfor> getShippinginforCollection() {
        return shippinginforCollection;
    }

    public void setShippinginforCollection(Collection<Shippinginfor> shippinginforCollection) {
        this.shippinginforCollection = shippinginforCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Customermember)) {
            return false;
        }
        Customermember other = (Customermember) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "edu.ptit.entity.Customermember[ id=" + id + " ]";
    }
    
}
