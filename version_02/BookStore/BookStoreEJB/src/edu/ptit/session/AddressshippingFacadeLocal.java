/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Addressshipping;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface AddressshippingFacadeLocal {

    void create(Addressshipping addressshipping);

    void edit(Addressshipping addressshipping);

    void remove(Addressshipping addressshipping);

    Addressshipping find(Object id);

    List<Addressshipping> findAll();

    List<Addressshipping> findRange(int[] range);

    int count();
    
}
