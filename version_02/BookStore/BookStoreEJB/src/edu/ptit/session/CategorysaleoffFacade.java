/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Categorysaleoff;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author NDT
 */
@Stateless
public class CategorysaleoffFacade extends AbstractFacade<Categorysaleoff> implements CategorysaleoffFacadeLocal {

    @PersistenceContext(unitName = "edu.ptit_ejbbookstore-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CategorysaleoffFacade() {
        super(Categorysaleoff.class);
    }
    
}
