/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Customernotmember;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface CustomernotmemberFacadeLocal {

    void create(Customernotmember customernotmember);

    void edit(Customernotmember customernotmember);

    void remove(Customernotmember customernotmember);

    Customernotmember find(Object id);

    List<Customernotmember> findAll();

    List<Customernotmember> findRange(int[] range);

    int count();
    
}
