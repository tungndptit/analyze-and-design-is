/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Customermember;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface CustomermemberFacadeLocal {

    void create(Customermember customermember);

    void edit(Customermember customermember);

    void remove(Customermember customermember);

    Customermember find(Object id);

    List<Customermember> findAll();

    List<Customermember> findRange(int[] range);

    int count();

    Customermember login(String username, String password);
}
