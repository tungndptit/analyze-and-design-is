/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Categorysaleoff;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface CategorysaleoffFacadeLocal {

    void create(Categorysaleoff categorysaleoff);

    void edit(Categorysaleoff categorysaleoff);

    void remove(Categorysaleoff categorysaleoff);

    Categorysaleoff find(Object id);

    List<Categorysaleoff> findAll();

    List<Categorysaleoff> findRange(int[] range);

    int count();
    
}
