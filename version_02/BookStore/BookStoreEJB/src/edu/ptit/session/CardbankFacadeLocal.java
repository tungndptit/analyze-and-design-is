/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Cardbank;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface CardbankFacadeLocal {

    void create(Cardbank cardbank);

    void edit(Cardbank cardbank);

    void remove(Cardbank cardbank);

    Cardbank find(Object id);

    List<Cardbank> findAll();

    List<Cardbank> findRange(int[] range);

    int count();
    
}
