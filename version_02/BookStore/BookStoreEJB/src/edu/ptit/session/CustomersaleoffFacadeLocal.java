/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Customersaleoff;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface CustomersaleoffFacadeLocal {

    void create(Customersaleoff customersaleoff);

    void edit(Customersaleoff customersaleoff);

    void remove(Customersaleoff customersaleoff);

    Customersaleoff find(Object id);

    List<Customersaleoff> findAll();

    List<Customersaleoff> findRange(int[] range);

    int count();
    
}
