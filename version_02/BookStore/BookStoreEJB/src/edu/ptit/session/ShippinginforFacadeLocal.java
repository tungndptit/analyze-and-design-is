/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Shippinginfor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface ShippinginforFacadeLocal {

    void create(Shippinginfor shippinginfor);

    void edit(Shippinginfor shippinginfor);

    void remove(Shippinginfor shippinginfor);

    Shippinginfor find(Object id);

    List<Shippinginfor> findAll();

    List<Shippinginfor> findRange(int[] range);

    int count();
    
}
