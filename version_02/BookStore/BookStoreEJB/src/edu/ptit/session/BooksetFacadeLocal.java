/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Bookset;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface BooksetFacadeLocal {

    void create(Bookset bookset);

    void edit(Bookset bookset);

    void remove(Bookset bookset);

    Bookset find(Object id);

    List<Bookset> findAll();

    List<Bookset> findRange(int[] range);

    int count();
    
}
