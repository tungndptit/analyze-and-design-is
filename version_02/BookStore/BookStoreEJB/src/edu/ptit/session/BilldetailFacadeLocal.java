/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Billdetail;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author NDT
 */
@Local
public interface BilldetailFacadeLocal {

    void create(Billdetail billdetail);

    void edit(Billdetail billdetail);

    void remove(Billdetail billdetail);

    Billdetail find(Object id);

    List<Billdetail> findAll();

    List<Billdetail> findRange(int[] range);

    int count();
    
}
