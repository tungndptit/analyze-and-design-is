/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Billdetail;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author NDT
 */
@Stateless
public class BilldetailFacade extends AbstractFacade<Billdetail> implements BilldetailFacadeLocal {

    @PersistenceContext(unitName = "edu.ptit_ejbbookstore-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BilldetailFacade() {
        super(Billdetail.class);
    }
    
}
