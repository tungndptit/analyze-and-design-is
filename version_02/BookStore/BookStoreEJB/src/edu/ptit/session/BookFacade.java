/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Book;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 *
 * @author NDT
 */
@Stateless
public class BookFacade extends AbstractFacade<Book> implements BookFacadeLocal {

    @PersistenceContext(unitName = "edu.ptit_ejbbookstore-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public BookFacade() {
        super(Book.class);
    }

    @Override
    public List<Book> searchByTitle(String title) {
        String hql = "SELECT b FROM Book b WHERE b.name LIKE :name";
        Query query = em.createQuery(hql);
        query.setParameter("name", "%"+title + "%");
        return query.getResultList();
    }

    @Override
    public List<Book> searchBySet(int idBookSet) {
        String hql =
                "SELECT b FROM Book b INNER JOIN b.bookSetID bs " +
                "WHERE bs.id = :idBookSet";
        return em.createQuery(hql).setParameter("idBookSet",idBookSet).getResultList();
    }

}
