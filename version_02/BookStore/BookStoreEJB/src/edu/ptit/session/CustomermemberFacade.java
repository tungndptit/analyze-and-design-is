/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.ptit.session;

import edu.ptit.entity.Customermember;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * @author NDT
 */
@Stateless
public class CustomermemberFacade extends AbstractFacade<Customermember> implements CustomermemberFacadeLocal {

    @PersistenceContext(unitName = "edu.ptit_ejbbookstore-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CustomermemberFacade() {
        super(Customermember.class);
    }

    @Override
    public Customermember login(String username, String password) {
        String hql = "SELECT cm " +
                "FROM Customermember cm INNER JOIN cm.accountID acc " +
                "WHERE acc.username = :user AND acc.password = :pass";
        Customermember cm = null;
        try {
            cm = (Customermember) em.createQuery(hql)
                    .setParameter("user", username)
                    .setParameter("pass", password)
                    .getSingleResult();

        } catch (Exception ex){
            cm = null;
        }
        return cm;
    }
}
