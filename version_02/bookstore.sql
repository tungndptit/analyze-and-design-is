-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: bookstore
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Username` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'tungnd','123456'),(2,'chienpq','123456'),(3,'vinhvh','123456');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Number` varchar(255) DEFAULT NULL,
  `Ward` varchar(255) DEFAULT NULL,
  `District` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'97A','Mỗ Lao','Mỗ Lao'),(2,'29','Nguyễn Chí Thanh','Hà Nội'),(3,'17','Nguyễn Thái Học','Hà Nội');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addressshipping`
--

DROP TABLE IF EXISTS `addressshipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressshipping` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `AddressID` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKAddressShi737506` (`AddressID`),
  CONSTRAINT `FKAddressShi737506` FOREIGN KEY (`AddressID`) REFERENCES `address` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addressshipping`
--

LOCK TABLES `addressshipping` WRITE;
/*!40000 ALTER TABLE `addressshipping` DISABLE KEYS */;
INSERT INTO `addressshipping` VALUES (1,1);
/*!40000 ALTER TABLE `addressshipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,'VP Bank'),(2,'Vietcom Bank'),(3,'DongA Bank');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bankaccount`
--

DROP TABLE IF EXISTS `bankaccount`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bankaccount` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CardBankID` int(10) NOT NULL,
  `BankID` int(10) NOT NULL,
  `CustomerID` int(10) NOT NULL,
  `Status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKBankAccoun420612` (`CustomerID`),
  KEY `FKBankAccoun110844` (`BankID`),
  KEY `FKBankAccoun305289` (`CardBankID`),
  CONSTRAINT `FKBankAccoun110844` FOREIGN KEY (`BankID`) REFERENCES `bank` (`ID`),
  CONSTRAINT `FKBankAccoun305289` FOREIGN KEY (`CardBankID`) REFERENCES `cardbank` (`ID`),
  CONSTRAINT `FKBankAccoun420612` FOREIGN KEY (`CustomerID`) REFERENCES `customermember` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bankaccount`
--

LOCK TABLES `bankaccount` WRITE;
/*!40000 ALTER TABLE `bankaccount` DISABLE KEYS */;
INSERT INTO `bankaccount` VALUES (1,1,1,1,'ok'),(2,2,1,2,'ok'),(3,3,2,3,'ok');
/*!40000 ALTER TABLE `bankaccount` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CartID` int(10) NOT NULL,
  `BankAccountID` int(10) NOT NULL,
  `Date` date DEFAULT NULL,
  `TotalPrice` float NOT NULL,
  `TotalCharge` float NOT NULL,
  `Status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKBill555191` (`BankAccountID`),
  KEY `FKBill412412` (`CartID`),
  CONSTRAINT `FKBill412412` FOREIGN KEY (`CartID`) REFERENCES `cart` (`ID`),
  CONSTRAINT `FKBill555191` FOREIGN KEY (`BankAccountID`) REFERENCES `bankaccount` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `billdetail`
--

DROP TABLE IF EXISTS `billdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billdetail` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BillID` int(10) NOT NULL,
  `PriceID` int(10) NOT NULL,
  `CategorySaleOffID` int(10) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `Price` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKBillDetail945877` (`CategorySaleOffID`),
  KEY `FKBillDetail456691` (`PriceID`),
  KEY `FKBillDetail542338` (`BillID`),
  CONSTRAINT `FKBillDetail456691` FOREIGN KEY (`PriceID`) REFERENCES `price` (`ID`),
  CONSTRAINT `FKBillDetail542338` FOREIGN KEY (`BillID`) REFERENCES `bill` (`ID`),
  CONSTRAINT `FKBillDetail945877` FOREIGN KEY (`CategorySaleOffID`) REFERENCES `categorysaleoff` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `billdetail`
--

LOCK TABLES `billdetail` WRITE;
/*!40000 ALTER TABLE `billdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `billdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BookSetID` int(10) NOT NULL,
  `CategoryID` int(10) NOT NULL,
  `PublisherID` int(10) NOT NULL,
  `Image` varchar(255) DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `AuthorName` varchar(255) DEFAULT NULL,
  `PublisherYear` int(10) NOT NULL,
  `Quantity` int(10) NOT NULL,
  `description` longtext,
  PRIMARY KEY (`ID`),
  KEY `FKBook221643` (`PublisherID`),
  KEY `FKBook738236` (`CategoryID`),
  KEY `FKBook694430` (`BookSetID`),
  CONSTRAINT `FKBook221643` FOREIGN KEY (`PublisherID`) REFERENCES `publisher` (`ID`),
  CONSTRAINT `FKBook694430` FOREIGN KEY (`BookSetID`) REFERENCES `bookset` (`ID`),
  CONSTRAINT `FKBook738236` FOREIGN KEY (`CategoryID`) REFERENCES `category` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,1,1,1,'hoathiencot.jpg','Hoa thiên cốt','Kim Dung',2014,100,'Nhà tiểu thuyết Kim Dung cho ra mắt chuyện dài tình cảm phương đông vào cuối năm 2014'),(2,4,2,2,'moiduatremotcachhoc.jpg','Mỗi đứa trẻ một cách học','Giáo dục',2014,10,'Mỗi đứa trẻ đều khác nhau không chỉ khác về vẻ bên ngoài mà còn khác về tư duy, nội tâm bên trong, chúng đón nhận thế giới trên nhiều góc nhìn khác nhau'),(3,2,1,3,'doremon001.jpg','Doreamon tập 1','Fujiko F Fujio',1996,50,'Truyện tranh dành cho thiếu nhi'),(4,2,1,3,'doremon006.jpg','Doreamon tập 6','Fujiko F Fujio',1996,20,'Truyện tranh dành cho thiếu nhi'),(5,1,1,2,'tuongtuongvadauvet.jpg','Tưởng Tượng Và Dấu Vết','Tưởng Tượng Và Dấu Vết',1987,15,'Cuốn sách về trí tưởng tượng và những điều bí ẩn trong cuộc sống'),(6,3,3,3,'dacnhantam.jpg','Đắc Nhân Tâm','Dale Carnegir',2005,10,'Không còn nữa khái niệm giới hạn Đắc Nhân Tâm là nghệ thuật thu phục lòng người, là làm cho tất cả mọi người yêu mến mình.'),(7,3,2,1,'songdongemdem.jpg','Sông đông êm đềm','Mikhail Solokhov',2003,9,'Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.'),(8,2,1,3,'nhoc-maruko.jpg','Maruko tập 1','Yoka',2011,16,'Truyện tranh dành cho thiếu nhi'),(9,2,1,3,'nhoc-maruko.jpg','Maruko tập 2','Yoka',2011,23,'Truyện tranh dành cho thiếu nhi'),(10,2,1,3,'nhoc-maruko.jpg','Maruko tập 3','Yoka',2011,23,'Truyện tranh dành cho thiếu nhi'),(11,2,1,3,'doiquannhinho13.jpg','Đội quân nhí nhố tập 13','Park In-seo',2014,21,'Truyện tranh dành cho thiếu nhi'),(12,2,1,3,'doiquannhinho5.jpg','Đội quân nhí nhố tập 5','Park In-seo',2014,21,'Truyện tranh dành cho thiếu nhi'),(13,2,1,3,'thandongdatviet35.jpg','Thần đồng đất Việt tập 35','Lê Linh',2012,20,'Truyện tranh dành cho thiếu nhi');
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookset`
--

DROP TABLE IF EXISTS `bookset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookset` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookset`
--

LOCK TABLES `bookset` WRITE;
/*!40000 ALTER TABLE `bookset` DISABLE KEYS */;
INSERT INTO `bookset` VALUES (1,'Truyện thiếu kiếm hiệp',NULL),(2,'Truyện thiếu nhi',NULL),(3,'Sách văn học',NULL),(4,'Sách giáo dục',NULL),(5,'Sách ngoại ngữ',NULL);
/*!40000 ALTER TABLE `bookset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardbank`
--

DROP TABLE IF EXISTS `cardbank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardbank` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CardType` varchar(255) DEFAULT NULL,
  `NameOwner` varchar(255) DEFAULT NULL,
  `CardNumber` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardbank`
--

LOCK TABLES `cardbank` WRITE;
/*!40000 ALTER TABLE `cardbank` DISABLE KEYS */;
INSERT INTO `cardbank` VALUES (1,'VIP','Nguyễn Duy Tùng','9101994'),(2,'VIP','Phạm Quang Chien','10101994'),(3,'VIP','Vương Hoàng Vinh','18031995');
/*!40000 ALTER TABLE `cardbank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT NULL,
  `CustomerID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Customer_idx` (`CustomerID`),
  CONSTRAINT `Customer` FOREIGN KEY (`CustomerID`) REFERENCES `customermember` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cartdetail`
--

DROP TABLE IF EXISTS `cartdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartdetail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `cartID` int(11) DEFAULT NULL,
  `bookID` int(11) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `cart_idx` (`cartID`),
  KEY `book_idx` (`bookID`),
  CONSTRAINT `book` FOREIGN KEY (`bookID`) REFERENCES `book` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cart` FOREIGN KEY (`cartID`) REFERENCES `cart` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartdetail`
--

LOCK TABLES `cartdetail` WRITE;
/*!40000 ALTER TABLE `cartdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `cartdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Truyện','Nhà xuất bản Kim Đồng'),(2,'Sách giáo dục','Nhà xuất bản giáo dục'),(3,'Sách văn học','Nhà xuất bản văn học');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorysaleoff`
--

DROP TABLE IF EXISTS `categorysaleoff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorysaleoff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CategoryID` int(10) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Percent` float NOT NULL,
  `MaxPrice` float NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKCategorySa229601` (`CategoryID`),
  CONSTRAINT `FKCategorySa229601` FOREIGN KEY (`CategoryID`) REFERENCES `category` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorysaleoff`
--

LOCK TABLES `categorysaleoff` WRITE;
/*!40000 ALTER TABLE `categorysaleoff` DISABLE KEYS */;
INSERT INTO `categorysaleoff` VALUES (1,1,'2016-05-12','2016-05-30',5,150),(2,2,'2016-05-11','2016-05-16',10,200);
/*!40000 ALTER TABLE `categorysaleoff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customermember`
--

DROP TABLE IF EXISTS `customermember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customermember` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `AccountID` int(10) NOT NULL,
  `PersonID` int(10) NOT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKCustomerMe209024` (`PersonID`),
  KEY `FKCustomerMe107200` (`AccountID`),
  CONSTRAINT `FKCustomerMe107200` FOREIGN KEY (`AccountID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKCustomerMe209024` FOREIGN KEY (`PersonID`) REFERENCES `person` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customermember`
--

LOCK TABLES `customermember` WRITE;
/*!40000 ALTER TABLE `customermember` DISABLE KEYS */;
INSERT INTO `customermember` VALUES (1,1,1,'tungnd@gmail.com','01677657899'),(2,2,2,'chienpq@gmail.com','01677657894'),(3,3,3,'vinhvh','01677657898');
/*!40000 ALTER TABLE `customermember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customernotmember`
--

DROP TABLE IF EXISTS `customernotmember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customernotmember` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `PersonID` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKCustomerNo864844` (`PersonID`),
  CONSTRAINT `FKCustomerNo864844` FOREIGN KEY (`PersonID`) REFERENCES `person` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customernotmember`
--

LOCK TABLES `customernotmember` WRITE;
/*!40000 ALTER TABLE `customernotmember` DISABLE KEYS */;
INSERT INTO `customernotmember` VALUES (1,4);
/*!40000 ALTER TABLE `customernotmember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customersaleoff`
--

DROP TABLE IF EXISTS `customersaleoff`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customersaleoff` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BankID` int(10) NOT NULL,
  `CustomerID` int(10) NOT NULL,
  `Price` float NOT NULL,
  `Percent` float NOT NULL,
  `MaxPrice` float NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKCustomerSa244426` (`CustomerID`),
  KEY `FKCustomerSa804291` (`BankID`),
  CONSTRAINT `FKCustomerSa244426` FOREIGN KEY (`CustomerID`) REFERENCES `customermember` (`ID`),
  CONSTRAINT `FKCustomerSa804291` FOREIGN KEY (`BankID`) REFERENCES `bank` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customersaleoff`
--

LOCK TABLES `customersaleoff` WRITE;
/*!40000 ALTER TABLE `customersaleoff` DISABLE KEYS */;
/*!40000 ALTER TABLE `customersaleoff` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `AccountID` int(10) NOT NULL,
  `PersonID` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKEmployee925855` (`PersonID`),
  KEY `FKEmployee390368` (`AccountID`),
  CONSTRAINT `FKEmployee390368` FOREIGN KEY (`AccountID`) REFERENCES `account` (`ID`),
  CONSTRAINT `FKEmployee925855` FOREIGN KEY (`PersonID`) REFERENCES `person` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fullname`
--

DROP TABLE IF EXISTS `fullname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fullname` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FName` varchar(255) DEFAULT NULL,
  `MName` varchar(255) DEFAULT NULL,
  `LName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fullname`
--

LOCK TABLES `fullname` WRITE;
/*!40000 ALTER TABLE `fullname` DISABLE KEYS */;
INSERT INTO `fullname` VALUES (1,'Nguyễn','Duy','Tùng'),(2,'Phạm ','Quang','Chiến'),(3,'Vương','Hoàng','Vinh'),(4,'Hoàng','Thanh','Nhạ');
/*!40000 ALTER TABLE `fullname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `ProviderID` int(10) NOT NULL,
  `Date` date DEFAULT NULL,
  `Command` varchar(255) DEFAULT NULL,
  `Status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKMessage623056` (`ProviderID`),
  CONSTRAINT `FKMessage623056` FOREIGN KEY (`ProviderID`) REFERENCES `provider` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `PaymentID` int(10) NOT NULL,
  `ShippingInforID` int(10) NOT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKOrders679905` (`ShippingInforID`),
  KEY `FKOrders520557` (`PaymentID`),
  CONSTRAINT `FKOrders520557` FOREIGN KEY (`PaymentID`) REFERENCES `payment` (`ID`),
  CONSTRAINT `FKOrders679905` FOREIGN KEY (`ShippingInforID`) REFERENCES `shippinginfor` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BillID` int(10) NOT NULL,
  `BankAccountID` int(10) NOT NULL,
  `State` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKPayment652940` (`BankAccountID`),
  KEY `FKPayment106663` (`BillID`),
  CONSTRAINT `FKPayment106663` FOREIGN KEY (`BillID`) REFERENCES `bill` (`ID`),
  CONSTRAINT `FKPayment652940` FOREIGN KEY (`BankAccountID`) REFERENCES `bankaccount` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `FullnameID` int(10) NOT NULL,
  `AddressID` int(10) NOT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Sex` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKPerson626786` (`AddressID`),
  KEY `FKPerson992965` (`FullnameID`),
  CONSTRAINT `FKPerson626786` FOREIGN KEY (`AddressID`) REFERENCES `address` (`ID`),
  CONSTRAINT `FKPerson992965` FOREIGN KEY (`FullnameID`) REFERENCES `fullname` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,1,1,'1994-10-09','Male'),(2,2,1,'1994-11-09','Male'),(3,3,1,'1993-12-09','Male'),(4,4,1,'1994-09-09','Male');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BookID` int(10) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `Price` float NOT NULL,
  `Status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKPrice909515` (`BookID`),
  CONSTRAINT `FKPrice909515` FOREIGN KEY (`BookID`) REFERENCES `book` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (1,1,'2016-05-12','2016-05-30',80000,NULL),(2,2,'2016-05-12','2016-05-30',85000,NULL),(3,3,'2016-05-12','2016-05-30',69000,NULL),(4,4,'2016-05-12','2016-05-30',69500,NULL),(5,5,'2016-05-12','2016-05-30',70000,NULL),(6,6,'2016-05-12','2016-05-30',72500,NULL),(7,7,'2016-05-12','2016-05-30',68500,NULL),(8,8,'2016-05-12','2016-05-30',68000,NULL),(9,9,'2016-05-12','2016-05-30',50000,NULL),(10,10,'2016-05-12','2016-05-30',150000,NULL),(11,11,'2016-05-12','2016-05-30',45000,NULL),(12,12,'2016-05-12','2016-05-30',60000,NULL),(13,13,'2016-05-12','2016-05-30',56000,NULL),(14,1,'2016-05-15','2016-05-18',75000,NULL);
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provider`
--

DROP TABLE IF EXISTS `provider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provider` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `AddressID` int(10) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKProvider345263` (`AddressID`),
  CONSTRAINT `FKProvider345263` FOREIGN KEY (`AddressID`) REFERENCES `address` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provider`
--

LOCK TABLES `provider` WRITE;
/*!40000 ALTER TABLE `provider` DISABLE KEYS */;
INSERT INTO `provider` VALUES (1,2,'Nhà cung cấp 1','ncc1@gmail.com','0123456789'),(2,3,'Nhà cung cấp 2','ncc2@gmail.com','0123456788');
/*!40000 ALTER TABLE `provider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `publisher` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `AddressID` int(10) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `CodePublisher` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKPublisher89685` (`AddressID`),
  CONSTRAINT `FKPublisher89685` FOREIGN KEY (`AddressID`) REFERENCES `address` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher`
--

LOCK TABLES `publisher` WRITE;
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
INSERT INTO `publisher` VALUES (1,2,'Nhà xuất bản truyện tranh','NXBTT'),(2,3,'Nhà xuất bản Giáo dục','NXBGD'),(3,2,'Nhà xuất bản Kim Đồng','NXBKD');
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quotation`
--

DROP TABLE IF EXISTS `quotation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quotation` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `BookID` int(10) NOT NULL,
  `MessageID` int(10) NOT NULL,
  `ProviderID` int(10) NOT NULL,
  `StartDate` date DEFAULT NULL,
  `EndDate` date DEFAULT NULL,
  `MaxQuantity` int(10) NOT NULL,
  `Status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKQuotation7686` (`ProviderID`),
  KEY `FKQuotation193121` (`MessageID`),
  KEY `FKQuotation203967` (`BookID`),
  CONSTRAINT `FKQuotation193121` FOREIGN KEY (`MessageID`) REFERENCES `message` (`ID`),
  CONSTRAINT `FKQuotation203967` FOREIGN KEY (`BookID`) REFERENCES `book` (`ID`),
  CONSTRAINT `FKQuotation7686` FOREIGN KEY (`ProviderID`) REFERENCES `provider` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quotation`
--

LOCK TABLES `quotation` WRITE;
/*!40000 ALTER TABLE `quotation` DISABLE KEYS */;
/*!40000 ALTER TABLE `quotation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `PersonID` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKSeller781231` (`PersonID`),
  CONSTRAINT `FKSeller781231` FOREIGN KEY (`PersonID`) REFERENCES `person` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller`
--

LOCK TABLES `seller` WRITE;
/*!40000 ALTER TABLE `seller` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippinginfor`
--

DROP TABLE IF EXISTS `shippinginfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shippinginfor` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `CustomerMemberID` int(10) NOT NULL,
  `AddressShippingID` int(10) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKShippingIn908317` (`AddressShippingID`),
  KEY `FKShippingIn772805` (`CustomerMemberID`),
  CONSTRAINT `FKShippingIn772805` FOREIGN KEY (`CustomerMemberID`) REFERENCES `customermember` (`ID`),
  CONSTRAINT `FKShippingIn908317` FOREIGN KEY (`AddressShippingID`) REFERENCES `addressshipping` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippinginfor`
--

LOCK TABLES `shippinginfor` WRITE;
/*!40000 ALTER TABLE `shippinginfor` DISABLE KEYS */;
/*!40000 ALTER TABLE `shippinginfor` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-16 20:17:07
