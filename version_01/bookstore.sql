CREATE DATABASE  IF NOT EXISTS `bookstore` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bookstore`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: bookstore
-- ------------------------------------------------------
-- Server version	5.7.10-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `address`
--

DROP TABLE IF EXISTS `address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address` (
  `idaddress` int(11) NOT NULL,
  `num` varchar(45) DEFAULT NULL,
  `ward` varchar(45) DEFAULT NULL,
  `distric` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idaddress`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address`
--

LOCK TABLES `address` WRITE;
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` VALUES (1,'97A','Mỗ Lao','Mỗ Lao','Hà Đông');
/*!40000 ALTER TABLE `address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addressshipping`
--

DROP TABLE IF EXISTS `addressshipping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressshipping` (
  `idaddressshipping` int(11) NOT NULL,
  `num` varchar(45) DEFAULT NULL,
  `ward` varchar(45) DEFAULT NULL,
  `distric` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idaddressshipping`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addressshipping`
--

LOCK TABLES `addressshipping` WRITE;
/*!40000 ALTER TABLE `addressshipping` DISABLE KEYS */;
/*!40000 ALTER TABLE `addressshipping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bank` (
  `idbank` int(11) NOT NULL,
  `balance` float DEFAULT NULL,
  `part` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idbank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES (1,0,'Kcoin');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill`
--

DROP TABLE IF EXISTS `bill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bill` (
  `idbill` int(11) NOT NULL,
  `idperson` int(11) NOT NULL,
  `idorders` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`idbill`),
  KEY `bill_seller_idx` (`idperson`),
  KEY `bill_orders_idx` (`idorders`),
  CONSTRAINT `bill_orders` FOREIGN KEY (`idorders`) REFERENCES `orders` (`idorders`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bill_seller` FOREIGN KEY (`idperson`) REFERENCES `seller` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill`
--

LOCK TABLES `bill` WRITE;
/*!40000 ALTER TABLE `bill` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `idbook` int(11) NOT NULL,
  `image` varchar(45) DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `author` varchar(45) DEFAULT NULL,
  `idcategory` int(11) NOT NULL,
  `publisher` varchar(45) DEFAULT NULL,
  `publishyear` varchar(45) DEFAULT NULL,
  `description` text,
  `originalprice` varchar(45) DEFAULT NULL,
  `saleprice` varchar(45) DEFAULT NULL,
  `idbookset` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  PRIMARY KEY (`idbook`),
  KEY `book_bookset_idx` (`idbookset`),
  KEY `book_category_idx` (`idcategory`),
  CONSTRAINT `book_bookset` FOREIGN KEY (`idbookset`) REFERENCES `bookset` (`idbookset`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `book_category` FOREIGN KEY (`idcategory`) REFERENCES `category` (`idcategory`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book`
--

LOCK TABLES `book` WRITE;
/*!40000 ALTER TABLE `book` DISABLE KEYS */;
INSERT INTO `book` VALUES (1,'hoathiencot.jpg','Hoa Thiên Cốt','Kim Dung',1,'Kim Dung','2014','Nhà tiểu thuyết Kim Dung cho ra mắt chuyện dài tình cảm phương đông vào cuối năm 2014','100000','96000',1,100),(2,'moiduatremotcachhoc.jpg','Mỗi Đứa Trẻ Một Cách Học','Cynthia Ulrich',1,'Cynthia Ulrich','2014','Mỗi đứa trẻ đều khác nhau không chỉ khác về vẻ bên ngoài mà còn khác về tư duy, nội tâm bên trong, chúng đón nhận thế giới trên nhiều góc nhìn khác nhau','80000','80000',1,100),(3,'1001cachlamgiau.jpg','1001 Cách Làm Giàu','Nam Hải',2,'No Name','2012','Làm giàu khó lắm','90000','70000',2,50),(4,'tuongtuongvadauvet.jpg','Tưởng Tượng Và Dấu Vết','Uông Triều',2,'Uông Triều','2014','Cuốn sách về trí tưởng tượng và những điều bí ẩn trong cuộc sống','100000','60000',2,0),(5,'doremon001.jpg','Doreamon tập 1','Fujiko F Fujio',1,'Kim Đồng','2014','Truyện tranh dành cho thiếu nhi','20000','18500',1,50),(6,'doremon006.jpg','Doreamon tập 6','Fujiko F Fujio',1,'Kim Đồng','2014','Truyện tranh dành cho thiếu nhi','20000','18500',1,50),(7,'doremon043.jpg','Doreamon tập 43','Fujiko F Fujio',1,'Kim Đồng','2014','Truyện tranh dành cho thiếu nhi','20000','18500',1,50),(8,'dacnhantam.jpg','Đắc Nhân Tâm','Dale Carnegir',2,'Kim Đồng','2006','Không còn nữa khái niệm giới hạn Đắc Nhân Tâm là nghệ thuật thu phục lòng người, là làm cho tất cả mọi người yêu mến mình.','60000','55000',3,10),(9,'songdongemdem.jpg','Sông đông êm đềm','Mikhail Solokhov',2,'Văn học','2009','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','57000',1,11),(10,'toithayhoavangtrencoxanh.jpg','Tôi thấy hoa vàng trên cỏ xanh','Nguyễn Vũ',1,'Đồng Nai','2010','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','56000',2,12),(11,'nhoc-maruko.jpg','Maruko tập 1','Yoka',2,'Giáo dục','2011','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','54500',3,13),(12,'nhoc-maruko.jpg','Maruko tập 2','Yoka',1,'Chính trị','2012','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','55500',4,12),(13,'nhoc-maruko.jpg','Maruko tập 3','Toka',2,'Thể thao','2013','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','54000',1,15),(14,'doiquannhinho13.jpg','Đội quân nhí nhố tập 13','Park In-seo',2,'Kim Đồng','2014','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','56000',2,16),(15,'doiquannhinho5.jpg','Đội quân nhí nhố tập 5','Park In-seo',2,'Lao Động','2015','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','57000',3,17),(16,'doiquannhinho21.jpg','Đội quân nhí nhố tập 21','Park In-seo',2,'Lao Động','2016','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','55000',4,18),(17,'doiquannhinho30.jpg','Đội quân nhí nhố tập 30','Park In-seo',2,'Lao Động','2017','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','60000','59900',1,19),(18,'thandongdatviet35.jpg','Thần đồng đất Việt tập 35','Lê Linh',1,'Kim Đồng','2018','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','70000','69000',2,20),(19,'thandongdatviet86.jpg','Thần đồng đất Việt tập 86','Lê Linh',1,'Kim Đồng','2019','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','75000','75000',3,21),(20,'thandongdatviet88.jpg','Thần đồng đất Việt tập 88','Lê Linh',1,'Kim Đồng','2020','Tác phẩm quá hay luôn, ko cần chê, nếu chê thì đọc lại cho đến khi ko chê được nữa.','85000','80000',4,22);
/*!40000 ALTER TABLE `book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookorder`
--

DROP TABLE IF EXISTS `bookorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookorder` (
  `idbookorder` int(11) NOT NULL,
  `idbook` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `idcart` int(11) NOT NULL,
  PRIMARY KEY (`idbookorder`),
  KEY `bookorder_book_idx` (`idbook`),
  KEY `bookorder_cart_idx` (`idcart`),
  CONSTRAINT `bookorder_book` FOREIGN KEY (`idbook`) REFERENCES `book` (`idbook`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `bookorder_cart` FOREIGN KEY (`idcart`) REFERENCES `cart` (`idcart`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookorder`
--

LOCK TABLES `bookorder` WRITE;
/*!40000 ALTER TABLE `bookorder` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookset`
--

DROP TABLE IF EXISTS `bookset`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookset` (
  `idbookset` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idbookset`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookset`
--

LOCK TABLES `bookset` WRITE;
/*!40000 ALTER TABLE `bookset` DISABLE KEYS */;
INSERT INTO `bookset` VALUES (1,'Truyện thiếu nhi'),(2,'Sách  kinh tế, tài chính'),(3,'Sách văn học'),(4,'Sách chính trị'),(5,'Sách ngoại ngữ');
/*!40000 ALTER TABLE `bookset` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardbank`
--

DROP TABLE IF EXISTS `cardbank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cardbank` (
  `idbank` int(11) NOT NULL,
  `cardtype` varchar(45) DEFAULT NULL,
  `nameowner` varchar(45) DEFAULT NULL,
  `numcard` varchar(45) DEFAULT NULL,
  `datereissue` date DEFAULT NULL,
  PRIMARY KEY (`idbank`),
  CONSTRAINT `cardbank_bank` FOREIGN KEY (`idbank`) REFERENCES `bank` (`idbank`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cardbank`
--

LOCK TABLES `cardbank` WRITE;
/*!40000 ALTER TABLE `cardbank` DISABLE KEYS */;
/*!40000 ALTER TABLE `cardbank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `idcart` int(11) NOT NULL,
  PRIMARY KEY (`idcart`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cartsave`
--

DROP TABLE IF EXISTS `cartsave`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cartsave` (
  `idcartsave` int(11) NOT NULL,
  `idcart` int(11) NOT NULL,
  `idperson` int(11) NOT NULL,
  PRIMARY KEY (`idcartsave`),
  KEY `cardsave_cart_idx` (`idcart`),
  KEY `cartsave_customermember_idx` (`idperson`),
  CONSTRAINT `cartsave_cart` FOREIGN KEY (`idcart`) REFERENCES `cart` (`idcart`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `cartsave_customermember` FOREIGN KEY (`idperson`) REFERENCES `customermember` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cartsave`
--

LOCK TABLES `cartsave` WRITE;
/*!40000 ALTER TABLE `cartsave` DISABLE KEYS */;
/*!40000 ALTER TABLE `cartsave` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `idcategory` int(11) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `area` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idcategory`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
INSERT INTO `category` VALUES (1,'Doanh nghiệp','Hà Nội','Nhà Xuất Bản Bừa Bãi'),(2,'Kinh Doanh','Hồ Chí Minh','Nhà Xuất Bản Tham Lam');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `idperson` int(11) NOT NULL,
  `phonenum` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idperson`),
  CONSTRAINT `customer_person` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` VALUES (1,'0986555666','tungnd1994@hotmail.com');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customermember`
--

DROP TABLE IF EXISTS `customermember`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customermember` (
  `idperson` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `idbank` int(11) NOT NULL,
  PRIMARY KEY (`idperson`),
  KEY `customermember_kcoinbank_idx` (`idbank`),
  CONSTRAINT `customermember_kcoinbank` FOREIGN KEY (`idbank`) REFERENCES `kcoinbank` (`idbank`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customermember`
--

LOCK TABLES `customermember` WRITE;
/*!40000 ALTER TABLE `customermember` DISABLE KEYS */;
INSERT INTO `customermember` VALUES (1,'tungnd','123456',1);
/*!40000 ALTER TABLE `customermember` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customernotmem`
--

DROP TABLE IF EXISTS `customernotmem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customernotmem` (
  `idperson` int(11) NOT NULL,
  PRIMARY KEY (`idperson`),
  CONSTRAINT `customernotmem_customer` FOREIGN KEY (`idperson`) REFERENCES `customer` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customernotmem`
--

LOCK TABLES `customernotmem` WRITE;
/*!40000 ALTER TABLE `customernotmem` DISABLE KEYS */;
/*!40000 ALTER TABLE `customernotmem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `idperson` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `phonenum` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idperson`),
  CONSTRAINT `employee_person` FOREIGN KEY (`idperson`) REFERENCES `person` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fullname`
--

DROP TABLE IF EXISTS `fullname`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fullname` (
  `idfullname` int(11) NOT NULL,
  `fname` varchar(45) DEFAULT NULL,
  `mname` varchar(45) DEFAULT NULL,
  `lname` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idfullname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fullname`
--

LOCK TABLES `fullname` WRITE;
/*!40000 ALTER TABLE `fullname` DISABLE KEYS */;
INSERT INTO `fullname` VALUES (1,'Nguyễn ','Duy ','Tùng');
/*!40000 ALTER TABLE `fullname` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kcoinbank`
--

DROP TABLE IF EXISTS `kcoinbank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kcoinbank` (
  `idbank` int(11) NOT NULL,
  PRIMARY KEY (`idbank`),
  CONSTRAINT `kcoinbank_bank` FOREIGN KEY (`idbank`) REFERENCES `bank` (`idbank`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kcoinbank`
--

LOCK TABLES `kcoinbank` WRITE;
/*!40000 ALTER TABLE `kcoinbank` DISABLE KEYS */;
INSERT INTO `kcoinbank` VALUES (1);
/*!40000 ALTER TABLE `kcoinbank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `idorders` int(11) NOT NULL,
  `idshippinginfor` int(11) NOT NULL,
  `idpayment` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`idorders`),
  KEY `orders_shippinginfor_idx` (`idshippinginfor`),
  KEY `orders_payment_idx` (`idpayment`),
  CONSTRAINT `orders_payment` FOREIGN KEY (`idpayment`) REFERENCES `payment` (`idpayment`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `orders_shippinginfor` FOREIGN KEY (`idshippinginfor`) REFERENCES `shippinginfor` (`idshippinginfor`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `payment` (
  `idpayment` int(11) NOT NULL,
  `idbank` int(11) NOT NULL,
  `idcart` int(11) NOT NULL,
  `state` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpayment`),
  KEY `payment_bank_idx` (`idbank`),
  KEY `payment_cart_idx` (`idcart`),
  CONSTRAINT `payment_bank` FOREIGN KEY (`idbank`) REFERENCES `bank` (`idbank`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `payment_cart` FOREIGN KEY (`idcart`) REFERENCES `cart` (`idcart`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `idperson` int(11) NOT NULL,
  `idfullname` int(11) NOT NULL,
  `idaddress` int(11) NOT NULL,
  `part` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idperson`),
  KEY `person_fullname_idx` (`idfullname`),
  KEY `person_address_idx` (`idaddress`),
  CONSTRAINT `person_address` FOREIGN KEY (`idaddress`) REFERENCES `address` (`idaddress`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `person_fullname` FOREIGN KEY (`idfullname`) REFERENCES `fullname` (`idfullname`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` VALUES (1,1,1,'Customer Member');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seller` (
  `idperson` int(11) NOT NULL,
  PRIMARY KEY (`idperson`),
  CONSTRAINT `seller_employee` FOREIGN KEY (`idperson`) REFERENCES `employee` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller`
--

LOCK TABLES `seller` WRITE;
/*!40000 ALTER TABLE `seller` DISABLE KEYS */;
/*!40000 ALTER TABLE `seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shippinginfor`
--

DROP TABLE IF EXISTS `shippinginfor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shippinginfor` (
  `idshippinginfor` int(11) NOT NULL,
  `idaddressshipping` int(11) NOT NULL,
  `customeridperson` int(11) NOT NULL,
  PRIMARY KEY (`idshippinginfor`),
  KEY `shippinginfor_addressshipping_idx` (`idaddressshipping`),
  KEY `shippinginfor_customer_idx` (`customeridperson`),
  CONSTRAINT `shippinginfor_addressshipping` FOREIGN KEY (`idaddressshipping`) REFERENCES `addressshipping` (`idaddressshipping`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `shippinginfor_customer` FOREIGN KEY (`customeridperson`) REFERENCES `customer` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shippinginfor`
--

LOCK TABLES `shippinginfor` WRITE;
/*!40000 ALTER TABLE `shippinginfor` DISABLE KEYS */;
/*!40000 ALTER TABLE `shippinginfor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staffmanager`
--

DROP TABLE IF EXISTS `staffmanager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staffmanager` (
  `idperson` int(11) NOT NULL,
  PRIMARY KEY (`idperson`),
  CONSTRAINT `staffmanager_employee` FOREIGN KEY (`idperson`) REFERENCES `employee` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staffmanager`
--

LOCK TABLES `staffmanager` WRITE;
/*!40000 ALTER TABLE `staffmanager` DISABLE KEYS */;
/*!40000 ALTER TABLE `staffmanager` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `staffstore`
--

DROP TABLE IF EXISTS `staffstore`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `staffstore` (
  `idperson` int(11) NOT NULL,
  PRIMARY KEY (`idperson`),
  CONSTRAINT `staffstore_employee` FOREIGN KEY (`idperson`) REFERENCES `employee` (`idperson`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `staffstore`
--

LOCK TABLES `staffstore` WRITE;
/*!40000 ALTER TABLE `staffstore` DISABLE KEYS */;
/*!40000 ALTER TABLE `staffstore` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-11  1:27:59
